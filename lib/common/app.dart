import 'package:flutter/material.dart';
import 'package:real/auth/presentation/pages/SignUp.dart';
import 'package:real/common/theme.dart';

import '../auth/presentation/pages/Main.dart';
import 'colors.dart';

class MyApp extends StatefulWidget {
  MyApp({super.key});

  var isLight = true;

  void changeTheme(BuildContext context){
    isLight = !isLight;
    context.findAncestorStateOfType<_MyAppState>()?.onChangedTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLight) ? lightColors : darkColors;
  }

  ThemeData getCurrentTheme(){
    return (isLight) ? theme : darkTheme;
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{
  void onChangedTheme(){
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: widget.getCurrentTheme(),
      home: const SignUp(),
    );
  }
}