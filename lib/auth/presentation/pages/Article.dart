import 'package:flutter/material.dart';

import '../../../common/app.dart';
import '../../data/models/ModelNews.dart';
import '../../data/models/PlatformModels.dart';
import 'MainPages/Home.dart';

class ArticlePage extends StatefulWidget {

  final ModelNews news;
  final ModelPlatform platform;

  const ArticlePage({super.key, required this.news, required this.platform});

  @override
  State<ArticlePage> createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {

  Duration currentDuration = Duration.zero;
  Duration? maxDuration;

  Widget getMediaContent(){
    var colors = MyApp.of(context).getColorsApp(context);
    if (widget.news.type == "audio"){
      return Column(
        children: [
          const SizedBox(height: 18),
          Container(
            width: double.infinity,
            color: colors.block,
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Row(
              children: [
                const SizedBox(width: 8),
                Image.asset("assets/prev_10.png"),
                const SizedBox(width: 8),
                const SizedBox(width: 8),
                Image.asset("assets/next_10.png"),
                const SizedBox(width: 8),
                Expanded(
                  child: SizedBox(
                    height: 10,
                    child: Slider(
                        value: currentDuration.inMilliseconds.toDouble(),
                        max: maxDuration?.inMilliseconds.toDouble() ?? 0,
                        onChanged: null
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Text(
                    (maxDuration == null )
                        ? "00:00/00:00"
                        : "${currentDuration.toString().substring(2, 7)}/${maxDuration.toString().substring(2, 7)}",
                    style: TextStyle(
                        color: colors.text,
                        fontSize: 10,
                        fontWeight: FontWeight.w500
                    )
                ),
                const SizedBox(width: 21)
              ],
            ),
          ),
        ],
      );
    }
    return const SizedBox();
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 63, left: 22, right: 22, bottom: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  GestureDetector(
                      onTap: (){
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (_) => const Home()
                            ),
                                (route) => false);
                      },
                      child: const Icon(Icons.arrow_back)
                  ),
                  const SizedBox(width: 14),
                  Image.network(widget.platform.getFullIconUrl(), height: 30, width: 30),
                  const SizedBox(width: 12),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                            text: TextSpan(
                                style: TextStyle(color: colors.text),
                                children: [
                                  TextSpan(
                                      text: "${widget.news.channel} • ",
                                      style: const TextStyle(
                                        fontSize: 16,
                                      )
                                  ),
                                  TextSpan(
                                      text: widget.platform.title,
                                      style: const TextStyle(fontSize: 12)),
                                ])),
                        Text(widget.news.formatDate(),
                            style: TextStyle(color: colors.subtext, fontSize: 10)),
                      ],
                    ),
                  ),
                  const Icon(Icons.share_outlined),
                  const SizedBox(width: 14),
                  const Icon(Icons.bookmark_border_outlined),
                ],
              ),
              const SizedBox(height: 18),
              Text(widget.news.title, style: TextStyle(
                  color: colors.text,
                  fontSize: 18,
                  height: 1,
                  fontWeight: FontWeight.bold
              )),
              getMediaContent(),
              const SizedBox(height: 18),
              Text(widget.news.text, style: TextStyle(
                  color: colors.text,
                  height: 1
              )),
            ],
          ),
        ),
      ),
    );
  }
}
