import 'package:flutter/material.dart';
import '../../../../common/app.dart';
import '../../../../common/utils.dart';
import '../../../data/models/ModelNews.dart';
import '../../../domain/HomePresenter.dart';
import '../Article.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {


  HomeTabPresenter presenter = HomeTabPresenter();
  int modeLayout = 0;

  Widget getItemNews(ModelNews news) {
    var colors = MyApp.of(context).getColorsApp(context);
    var platform = news.getModelPlatform(presenter.platform);
    return GestureDetector(
      onTap: (){
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (_) => ArticlePage(news: news, platform: platform)
            ),
                (route) => false
        );
      },
      child: Column(
        children: [
          const SizedBox(height: 12),
          Row(
            children: [
              Image.network(platform.getFullIconUrl(), height: 30, width: 30),
              const SizedBox(width: 12),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                      text: TextSpan(
                          style: TextStyle(color: colors.text),
                          children: [
                            TextSpan(
                                text: "${news.channel} • ",
                                style: const TextStyle(
                                  fontSize: 16,
                                )
                            ),
                            TextSpan(
                                text: platform.title,
                                style: const TextStyle(fontSize: 12)),
                          ])),
                  Text(news.formatDate(),
                      style: TextStyle(color: colors.subtext, fontSize: 10))
                ],
              )
            ],
          ),
          const SizedBox(height: 12),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(news.getTitle(modeLayout),
                    style: TextStyle(
                        color: colors.text,
                        fontWeight: FontWeight.w500,
                        height: 1
                    ),
                  )
              ),
              const SizedBox(width: 8),
              (news.type == "photo" && modeLayout == 0) ? SizedBox(
                height: 75,
                child: AspectRatio(
                  aspectRatio: 4/3,
                  child: Image.network(
                    news.media!, fit: BoxFit.cover,
                  ),
                ),
              ) : (news.type == "link" && modeLayout == 0) ? Container(
                height: 75,
                width: 100,
                color: colors.block,
                child: Image.asset("assets/link.png"),
              ) : (news.type == "audio" && modeLayout == 0) ? Container(
                height: 75,
                width: 100,
                color: colors.block,
                child: Image.asset("assets/play.png"),
              ) : const SizedBox()
            ],
          ),
          const SizedBox(height: 12),
          Text(
              news.getText(modeLayout),
              style: TextStyle(
                  fontSize: 12,
                  color: colors.text
              )
          ),
          const SizedBox(height: 12),
          Divider(height: 1, color: colors.text),
          const SizedBox(height: 12),
        ],
      ),
    );
  }

  Widget getMediaItemNews(ModelNews news) {
    var platform = news.getModelPlatform(presenter.platform);
    return GestureDetector(
        onTap: (){
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (_) => ArticlePage(news: news, platform: platform)
              ),
                  (route) => false
          );
        },
        child: Column(
          children: [
            Image.network(news.media!),
            const SizedBox(height: 4),
          ],
        )
    );
  }

  List<Widget> getItems() {
    if (modeLayout != 2) {
      return presenter.news.map((e) => getItemNews(e)).toList();
    }else{
      var onlyNewsWithMedia = presenter.news.where((element) => element.type == "photo").toList();
      var firstColumn = onlyNewsWithMedia.where((element) => onlyNewsWithMedia.indexOf(element) % 2 == 0).toList();
      var secondColumn = onlyNewsWithMedia.where((element) => onlyNewsWithMedia.indexOf(element) % 2 != 0).toList();
      return [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                children: firstColumn.map((e) => getMediaItemNews(e)).toList(),
              ),
            ),
            const SizedBox(width: 4),
            Expanded(
              child: Column(
                children: secondColumn.map((e) => getMediaItemNews(e)).toList(),
              ),
            )
          ],
        )
      ];
    }
  }

  void changeLayout(int newMode){
    setState(() {
      modeLayout = newMode;
    });
    Navigator.pop(context);
    showPickerLayoutDialog();
  }

  void showPickerLayoutDialog(){
    var colors = MyApp.of(context).getColorsApp(context);
    showDialog(context: context, builder: (_) => Dialog(
      surfaceTintColor: Colors.transparent,
      insetPadding: const EdgeInsets.symmetric(horizontal: 22),
      backgroundColor: colors.block,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12))
      ),
      child: SizedBox(
        width: double.infinity,
        height: 248,
        child: Padding(
          padding: const EdgeInsets.only(top: 18, bottom: 28, left: 22, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("СЕТКА ОТОБРАЖЕНИЯ", style: TextStyle(
                color: colors.subtext,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              )),
              const SizedBox(height: 16),
              Divider(height: 1, color: colors.subtext, endIndent: 2),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(0);
                },
                child: Row(
                  children: [
                    (modeLayout == 0) ? Image.asset("assets/mode_0_check.png") : Image.asset("assets/mode_0.png"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Card", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (modeLayout == 0) ? FontWeight.bold : FontWeight.normal
                      )),
                    ),

                  ],
                ),
              ),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(1);
                },
                child: Row(
                  children: [
                    (modeLayout == 1) ? Image.asset("assets/mode_1_check.png") : Image.asset("assets/mode_1.png"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Text", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (modeLayout == 1) ? FontWeight.bold : FontWeight.normal
                      )),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 28),

            ],
          ),
        ),
      ),
    ));
  }

  @override
  void initState() {
    super.initState();
    presenter.fetchData(
            (res) => setState(() {
              presenter.news = res;
            }),
            (error) => showError(context, error)
    );
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: (modeLayout != 2) ? 22 : 4),
          child: Column(
              children: <Widget>[
                const SizedBox(height: 63),
                Row(
                  children: [
                    SizedBox(width: (modeLayout == 2) ? 18 : 0),
                    Expanded(
                        child: Text("Лента новостей",
                            style: TextStyle(
                                color: colors.text,
                                fontSize: 24,
                                fontWeight: FontWeight.bold
                            )
                        )
                    ),
                    GestureDetector(
                        onTap: () {
                          showPickerLayoutDialog();
                        },
                        child: Image.asset("assets/mode_$modeLayout.png")),
                    SizedBox(width: (modeLayout == 2) ? 18 : 0),
                  ],
                ),
                const SizedBox(height: 18),
              ] + getItems()),
        ),
      ),
    );
  }

}