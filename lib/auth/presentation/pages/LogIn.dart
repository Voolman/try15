import 'package:flutter/material.dart';
import 'package:real/auth/domain/LogInPresenter.dart';
import 'package:real/auth/presentation/pages/ForgotPassword.dart';
import 'package:real/auth/presentation/pages/SetupProfile.dart';
import 'package:real/auth/presentation/pages/SignUp.dart';
import 'package:real/common/widgets/CustomTextField.dart';

import '../../../common/colors.dart';
import '../../../common/utils.dart';
import 'Holder.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}
var colors = LightColors();
class _LogInState extends State<LogIn> {
  bool isChecked = false;
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.only(top: 83, left: 24,right: 24),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  'Добро пожаловать',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
            const SizedBox(height: 8),
            Row(
              children: [
                Text(
                  'Заполните почту и пароль чтобы продолжить',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ],
            ),
            const SizedBox(height: 4,),
            CustomTextField(
                label: 'Почта',
                hint: '***********@mail.com',
                controller: email,
                onChanged: onChanged
            ),
            CustomTextField(
                label: 'Пароль',
                hint: '**********',
                controller: password,
                enableObscure: true,
                onChanged: onChanged
            ),
            const SizedBox(height: 18,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      height: 22,
                      width: 22,
                      child: Checkbox(
                        value: isChecked,
                        activeColor: colors.accent,
                        checkColor: colors.background,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                        ),
                        side: BorderSide(color: colors.subtext, width: 1),
                        onChanged: (value) { 
                          setState(() {
                            isChecked = value!;
                          });
                        },
                      ),
                    ),
                    const SizedBox(width: 8),
                    Text(
                      'Запомнить меня',
                      style: Theme.of(context).textTheme.titleMedium,
                    )
                  ],
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ForgotPassword()));
                  },
                  child: Text(
                    'Забыли пароль?',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent),
                  ),
                )
              ],
            ),
            const SizedBox(height: 371),
            SizedBox(
              height: 46,
              width: double.infinity,
              child: FilledButton(
                  onPressed: (){
                    pressSignIn(email.text, password.text,
                        (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const SetupProfilePage()));},
                            (String e){showError(context, e);}
                    );
                  },
                  style: Theme.of(context).filledButtonTheme.style,
                  child: Text(
                    'Войти',
                    style: Theme.of(context).textTheme.labelMedium,
                  )
              ),
            ),
            const SizedBox(height: 14),
            GestureDetector(
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUp()));
              },
              child: RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'У меня нет аккаунта! ',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                        ),
                        TextSpan(
                            text: 'Создать',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                        )
                      ]
                  )
              ),
            )
          ],
        ),
      ),
    );
  }
}