import 'package:flutter/material.dart';
import 'package:real/auth/domain/SignUpPresenter.dart';
import 'package:real/auth/presentation/pages/LogIn.dart';
import 'package:real/auth/presentation/pages/SetupProfile.dart';
import 'package:real/common/widgets/CustomTextField.dart';

import '../../../common/colors.dart';
import '../../../common/utils.dart';
import 'Holder.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}
var colors = LightColors();
class _SignUpState extends State<SignUp> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.only(top: 83, left: 24,right: 24),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  'Создать аккаунт',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
            const SizedBox(height: 8),
            Row(
              children: [
                Text(
                  'Завершите регистрацию чтобы начать',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ],
            ),
            const SizedBox(height: 4,),
            CustomTextField(
                label: 'Почта',
                hint: '***********@mail.com',
                controller: email,
                onChanged: onChanged
            ),
            CustomTextField(
                label: 'Пароль',
                hint: '**********',
                controller: password,
                enableObscure: true,
                onChanged: onChanged
            ),
            CustomTextField(
                label: 'Повторите пароль',
                hint: '**********',
                controller: confirmPassword,
                enableObscure: true,
                onChanged: onChanged
            ),
            const SizedBox(height: 319),
            SizedBox(
              height: 46,
              width: double.infinity,
              child: FilledButton(
                  onPressed: (){
                    pressSignUp(
                        email.text,
                        password.text,
                        (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const SetupProfilePage()));},
                            (String e){showError(context, e);}
                    );
                  },
                  style: Theme.of(context).filledButtonTheme.style,
                  child: Text(
                    'Зарегистрироваться',
                    style: Theme.of(context).textTheme.labelMedium,
                  )
              ),
            ),
            const SizedBox(height: 14),
            GestureDetector(
              onTap: (){
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
              },
              child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'У меня уже есть аккаунт! ',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                      ),
                      TextSpan(
                        text: 'Войти',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                      )
                    ]
              )
              ),
            )
          ],
        ),
      ),
    );
  }
}