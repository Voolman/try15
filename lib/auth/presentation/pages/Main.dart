import 'package:flutter/material.dart';
import 'package:real/auth/presentation/pages/MainPages/Favorite.dart';
import 'package:real/auth/presentation/pages/MainPages/Home.dart';
import 'package:real/auth/presentation/pages/MainPages/Profile.dart';
import 'package:real/auth/presentation/pages/MainPages/Search.dart';
import 'package:real/common/app.dart';

class Main extends StatefulWidget {
  const Main({super.key});

  @override
  State<Main> createState() => _MainState();
}

var currentIndex = 0;


class _MainState extends State<Main> {
  @override
  void initState() {
    super.initState();
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
          backgroundColor: colors.background,
          currentIndex: currentIndex,
          elevation: 0,
          showUnselectedLabels: true,
          selectedItemColor: colors.accent,
          unselectedItemColor: colors.subtext,
          selectedLabelStyle: Theme.of(context).bottomNavigationBarTheme.selectedLabelStyle,
          unselectedLabelStyle: Theme.of(context).bottomNavigationBarTheme.unselectedLabelStyle,
          items: [
            BottomNavigationBarItem(icon: (currentIndex == 0) ? const Icon(Icons.home): const Icon(Icons.home_outlined), label: 'Home'),
            const BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
            BottomNavigationBarItem(icon: (currentIndex == 2) ? const Icon(Icons.bookmark) : const Icon(Icons.bookmark_outline), label: 'Favorite'),
            BottomNavigationBarItem(icon: (currentIndex == 3) ? Image.asset("assets/sel_profile.png"): Image.asset('assets/profile.png'), label: 'Profile'),
          ],
        onTap: (newIndex){
          setState(() {
            currentIndex = newIndex;
          });
        },
      ),
      body: [const Home(), const Search(), const Favorite(), const Profile()][currentIndex],
    );
  }
}