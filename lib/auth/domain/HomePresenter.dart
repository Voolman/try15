import 'package:real/auth/data/models/PlatformModels.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/models/ModelNews.dart';
import '../data/repository/supabase.dart';

class HomeTabPresenter {
  List<ModelPlatform> platform = [];
  List<ModelNews> news = [];

  void fetchData(
    Function(List<ModelNews>) onResponse,
    Function(String) onError,
  ) {
    getAllPlatforms().then((value) async {
      platform = value;
      try {
        var result = await getNews(value);
          onResponse(result);
      } on AuthException catch (e) {
        onError(e.message);
      }
    });
  }
}
