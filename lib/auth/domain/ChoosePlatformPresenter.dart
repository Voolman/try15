import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/models/PlatformModels.dart';
import '../data/repository/supabase.dart';

class ChoosePlatformPresenter {

  Map<ModelPlatform, bool> platforms = {};

  void unselectPlatform(ModelPlatform platform){
    platforms[platform] = false;
  }

  void selectPlatform(ModelPlatform platform){
    platforms[platform] = true;
  }

  List<ModelPlatform> getSelectedPlatform(){
    return platforms.keys.where((key) => platforms[key]!).toList();
  }

  Future<void> fetchListPlatforms(
      List<ModelPlatform> alreadySelectPlatforms,
      Function(Map<ModelPlatform, bool>) onResponse,
      Function(String) onError,
      ) async {
    try{
      getAllPlatforms;
          (List<ModelPlatform> response) {
        var alreadySelectIdsMap = {};
        for (var model in alreadySelectPlatforms) {
          alreadySelectIdsMap[model.id] = model;
        }

        Map<ModelPlatform, bool> data = {};
        for (var platform in response) {
          ModelPlatform e;
          bool isAlreadyHas = alreadySelectIdsMap.keys.contains(platform.id);
          if (isAlreadyHas) {
            e = alreadySelectIdsMap[platform.id]!;
          } else {
            e = platform;
          }
          data[e] = isAlreadyHas;
        }
        onResponse(data);
      };
  }on AuthException catch(e) {
      onError(e.message);
    }
  }
}

Future<void> pressSave(Function onResponse) async {
  try{
    List<ModelPlatform> platforms = await getAllPlatforms();
    await savePlatforms(platforms);
    onResponse();
  }on AuthException catch(e){}
}