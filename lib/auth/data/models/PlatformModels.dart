class ModelPlatform {
  final String id;
  final String icon;
  final String title;
  final String defaultChannels;
  final String availableChannels;
  String channels;

  ModelPlatform(
      {
        required this.id,
        required this.icon,
        required this.title,
        required this.defaultChannels,
        required this.availableChannels
      }
      ): channels = defaultChannels;

  bool isValid(){
    var channelsUser = channels.split(", ");
    return availableChannels
        .split(", ")
        .toSet()
        .containsAll(channelsUser);
  }

  String getFullIconUrl(){
    return "https://kxztakjpguqeupssdhzm.supabase.co/storage/v1/object/public/icons/$icon";
  }

  static ModelPlatform fromJson(Map<String, dynamic> json){
    return ModelPlatform(
        id: json["id"],
        icon: json["icon"],
        title: json["title"],
        defaultChannels: json["default_channels"],
        availableChannels: json["allow_channels"]
    );
  }

  Map<String, String> toJson(){
    return {
      "id": id,
      "icon": icon,
      "title": title,
      "default_channels": defaultChannels,
      "allow_channels": availableChannels
    };
  }
}
