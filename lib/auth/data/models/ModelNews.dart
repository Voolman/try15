import 'dart:ffi';

import 'package:intl/intl.dart';

import 'PlatformModels.dart';

class ModelNews {
  final String title;
  final String text;
  final String? media;
  final String type;
  final String channel;
  final String idPlatform;
  final DateTime publishedAt;
  final Double? geoLat;
  final Double? geoLong;

  ModelNews(
      {required this.title,
        required this.text,
        this.media,
        required this.type,
        required this.channel,
        required this.idPlatform,
        required this.publishedAt,
        this.geoLat,
        this.geoLong});

  static ModelNews fromJson(Map<String, dynamic> json) {
    return ModelNews(
      title: json["title"].toString(),
      text: json["text"].toString(),
      media: json["media"].toString(),
      type: json["type_media"],
      channel: json["channel"],
      idPlatform: json["id_platform"],
      publishedAt:
      DateFormat("yyyy-MM-ddTHH:mm:ss.SSSSSS").parse(json["published_at"]),
      geoLat: json["geo_lat"],
      geoLong: json["geo_long"],
    );
  }

  ModelPlatform getModelPlatform(List<ModelPlatform> platforms) {
    return platforms.where((element) => element.id == idPlatform).single;
  }

  String formatDate() {
    return DateFormat("dd MMM, HH:mm", "ru")
        .format(publishedAt)
        .replaceAll(".", "");
  }

  String getTitle(int mode) {
    if (mode == 0) {
      return (title.length > 250) ? "${title.substring(0, 250)}..." : title;
    }
    return (title.length > 500) ? "${title.substring(0, 500)}..." : title;
  }

  String getText(int mode) {
    if (mode == 0) {
      return (text.length > 500) ? "${text.substring(0, 500)}..." : text;
    }
    return (text.length > 1500) ? "${text.substring(0, 1500)}..." : text;
  }
}
