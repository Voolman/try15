import 'dart:typed_data';

import 'package:supabase_flutter/supabase_flutter.dart';

import '../models/ModelNews.dart';
import '../models/PlatformModels.dart';
import '../models/ProfileModels.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String email, String password) async {
  return await supabase.auth.signUp(email: email, password: password);
}

Future<AuthResponse> signIn(String email, String password) async {
  return await supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return await supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return await supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) async {
  return await supabase.auth.verifyOTP(email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) async {
  return await supabase.auth.updateUser(UserAttributes(
      password: password
  ));
}

bool checkExistPlatformsMetadata(){
  return supabase.auth.currentUser?.userMetadata?.containsKey("platforms") ?? false;
}

Future<void> uploadAvatar(Uint8List bytes) async {
  var name = "${supabase.auth.currentUser!.id}.png";
  await supabase.storage
      .from("avatars")
      .uploadBinary(
      name,
      bytes
  );
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": name
          }
      )
  );
}

Future<void> removeAvatar() async {
  await supabase.storage
      .from("avatars")
      .remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": null
          }
      )
  );
}

Future<void> saveUserData(
    String fullname,
    String phone,
    String birthday
    ) async {
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "fullname": fullname,
            "phone": phone,
            "birthday": birthday,
          }
      )
  );
}

ModelProfile getModelProfile() {
  var userMetadata = supabase.auth.currentUser?.userMetadata!;
  var platforms = loadPlatforms();
  return ModelProfile(
      fullname: userMetadata?["fullname"],
      phone: userMetadata?["phone"],
      birthday: userMetadata?["birthday"],
      avatar: userMetadata?["avatar"],
      platforms: platforms
  );
}

Future<UserResponse> savePlatforms(List<ModelPlatform> platforms) async {
  return await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "platforms": platforms.map(
                    (e) => e.toJson()
            ).toList()
          }
      )
  );
}

Future<List<ModelPlatform>> getAllPlatforms() async {
  var response = await supabase
      .from("platforms")
      .select();
  return response.map(
          (Map<String, dynamic> e) => ModelPlatform.fromJson(e)
  ).toList();
}

Future<ModelPlatform> getPlatform(String id) async {
  var response = await supabase
      .from("platforms")
      .select()
      .eq("id", id)
      .single();
  return ModelPlatform.fromJson(response);
}

bool checkExistAvatarMetadata() {
  return supabase.auth.currentUser?.userMetadata?["avatar"] != null;
}

List<ModelPlatform> loadPlatforms() {
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!["platforms"];

  List<ModelPlatform> platforms = shortData.map((e) =>
      ModelPlatform.fromJson(e)
  ).toList();

  return platforms;
}

Future<List<ModelNews>> getNews(List<ModelPlatform> filters) async {
  List<String> channels = [];
  for(var platform in filters){
    channels += platform.channels.split(", ");
  }
  var response = await supabase.from("news").select();
  var result = response.map((e) => ModelNews.fromJson(e)).toList();
  print(result);
  return result;
}
